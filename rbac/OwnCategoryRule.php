<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii;
use app\models\User;

class OwnCategoryRule extends Rule
{
	public $name = 'ownCategoryRule';

	public function execute($user, $item, $params)
	{
		$userModel=User::findIdentity($user);
		
		if (!Yii::$app->user->isGuest) {
			return isset($params['activity']) ? $params['activity']->categoryId == $userModel->categoryId : false;
		}
		return false;
	}
}