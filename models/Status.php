<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "status".
 *
 * @property integer $id
 * @property string $name
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
	
	public static function getStatuses()
	{
		$statuses = ArrayHelper::
					map(self::find()->all(), 'id', 'name');
		return $statuses;						
	}
	
	
	
	public static function getStatusesWithAllStatuses()
	{
		$allStatuses = self::getStatuses();
		$allStatuses[-1] = 'All Statuses';
		$allStatuses = array_reverse ( $allStatuses, true );
		return $allStatuses;	
	}
	
	public function getName()
    {
        return $this->name;
    }

    /**
     * @inheritdoc
     */
    
	
	
}
