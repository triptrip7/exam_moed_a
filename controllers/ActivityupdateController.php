<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;


class ActivityupdateController extends Controller
{
	public function actionAddrule()
	{
		$auth = Yii::$app->authManager;				
		$updateOwnCategoryActivities = $auth->getPermission('updateOwnCategoryActivities');
		
		$rule = new \app\rbac\OwnCategoryRule;
		$auth->add($rule);
				
		$updateOwnCategoryActivities->ruleName = $rule->name;		
		$auth->add($updateOwnCategoryActivities);	
	}
}